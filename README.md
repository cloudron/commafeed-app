Building
========
docker build -t girish/commafeed .

Running
=======
docker run -ti -P --env-file envfile --link postgresql:postgresql girish/commafeed 

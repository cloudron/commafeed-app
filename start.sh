#!/bin/bash

set -eu -o pipefail

cd /app/code

readonly fqdn=$(hostname -f)

sed -e "s/##FQDN/${fqdn}/" \
    -e "s/##MAIL_SMTP_SERVER/${MAIL_SMTP_SERVER}/" \
    -e "s/##MAIL_SMTP_USERNAME/${MAIL_SMTP_USERNAME}/" \
    -e "s/##MAIL_FROM/${MAIL_SMTP_USERNAME}@${MAIL_DOMAIN}/" \
    -e "s,##JDBC_URL,postgresql://${POSTGRESQL_HOST}:${POSTGRESQL_PORT}/${POSTGRESQL_DATABASE}," \
    -e "s/##POSTGRESQL_USERNAME/${POSTGRESQL_USERNAME}/" \
    -e "s/##POSTGRESQL_PASSWORD/${POSTGRESQL_PASSWORD}/" \
    /app/code/config.yml.template > /app/code/config.yml

java -jar target/commafeed.jar server config.yml


FROM cloudron/base:0.5.0
MAINTAINER Girish Ramakrishnan <girish@forwardbias.in>

RUN sudo adduser \
   --system \
   --shell /bin/bash \
   --disabled-password \
   --home /app/code \
   commafeed

WORKDIR /app/code

RUN update-java-alternatives -s java-1.8.0-openjdk-amd64

USER commafeed
RUN curl -L https://github.com/Athou/commafeed/tarball/891f660738264670cb43b995fb7c72de52c001a4 | tar -xz --strip-components 1 -f -

ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
RUN PATH=$PATH:$JAVA_HOME/bin mvn clean package

ADD config.yml.template /app/code/config.yml.template
ADD start.sh /app/code/start.sh

EXPOSE 8082

CMD [ "/app/code/start.sh" ]
